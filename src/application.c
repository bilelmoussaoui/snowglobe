// SPDX-FileCopyrightText: Red Hat, Inc.
// SPDX-License-Identifier: GPL-3.0-or-later

#include "config.h"

#include <dlfcn.h>

#include "application.h"
#include "window.h"

struct _SnowglobeApplication
{
  GtkApplication parent_instance;
  gboolean is_decorated;
  gboolean emulate_touch;
  gboolean show_host_cursor;
  gboolean auto_resize;
  gboolean is_fullscreened;

  guint main_window_id;
  /* Subprocess state */
  guint watcher_id;
  gboolean process_started;
  GSubprocess      *proc;
  gchar           **argv;
};

G_DEFINE_TYPE (SnowglobeApplication, snowglobe_application, GTK_TYPE_APPLICATION)

static void
on_name_appeared_cb (GDBusConnection  *connection,
                     const gchar      *name,
                     const gchar      *name_owner,
                     gpointer          user_data)
{
  SnowglobeApplication *self = user_data;
  GtkWindow *window;
  g_assert (SNOWGLOBE_IS_APPLICATION (self));
  g_assert (G_IS_DBUS_CONNECTION (connection));
  window = gtk_application_get_window_by_id (GTK_APPLICATION (self),
                                             self->main_window_id);

  self->process_started = TRUE;
  snowglobe_window_set_display_view (SNOWGLOBE_WINDOW (window), connection);
}

static void
on_name_vanished_cb (GDBusConnection  *connection,
                     const gchar      *name,
                     gpointer          user_data)
{
  SnowglobeApplication *self = user_data;
  GtkWindow *window, *next_window;
  GList *windows, *next;
  g_assert (SNOWGLOBE_IS_APPLICATION (self));
  g_assert (G_IS_DBUS_CONNECTION (connection));

  window = gtk_application_get_window_by_id (GTK_APPLICATION (self),
                                             self->main_window_id);
  windows = gtk_application_get_windows (GTK_APPLICATION (self));
  /**
   * We need to know if we already started the process or not
   * as the vanished_cb might get called the moment the watcher is registered
   * and we don't want to display an error when the qemu process is starting.
   */
  if (self->process_started)
    {
      snowglobe_window_set_status_view (SNOWGLOBE_WINDOW (window), NULL,
                                        _("Process Stopped"),
                                        _("The DBus name is no longer available"));
      if (self->proc)
        {
          g_subprocess_force_exit (self->proc);
          self->proc = NULL;
        }

      while (windows)
        {
          next_window = windows->data;
          next = windows->next;
          if (gtk_application_window_get_id (GTK_APPLICATION_WINDOW (next_window)) != self->main_window_id)
            {
              gtk_application_remove_window (GTK_APPLICATION (self), next_window);
              gtk_window_close (GTK_WINDOW (next_window));
            }

          windows = next;
        }
    }
  else
    snowglobe_window_set_loading_view (SNOWGLOBE_WINDOW (window));
}

static void
start_qemu (SnowglobeApplication *self)
{
  GtkWindow *window;
  GSubprocess *proc;
  g_autoptr (GError) error = NULL;
  g_assert (SNOWGLOBE_IS_APPLICATION (self));

  if (self->argv && g_strv_length (self->argv) > 0)
    {
      g_info ("Starting qemu subprocess");
      proc = g_subprocess_newv ((const gchar *const *)self->argv,
                                G_SUBPROCESS_FLAGS_STDERR_MERGE, &error);
      if (error != NULL)
        {
          window = gtk_application_get_window_by_id (GTK_APPLICATION (self),
                                                     self->main_window_id);
          snowglobe_window_set_status_view (SNOWGLOBE_WINDOW (window), NULL,
                                            _("Failed to start QEMU"),
                                            error->message);
          return;
        }
      else
        self->proc = proc;
    }
  g_info ("Starting bus watcher");
  self->watcher_id = g_bus_watch_name (G_BUS_TYPE_SESSION, "org.qemu",
                                       G_BUS_NAME_WATCHER_FLAGS_NONE,
                                       on_name_appeared_cb,
                                       on_name_vanished_cb,
                                       self,
                                       g_object_unref);
}

static void
snowglobe_application_set_dark_style (void)
{
  void *adw;
  adw = dlopen ("libadwaita-1.so.0", RTLD_LAZY);
  if (adw)
    {
      void (* adw_init) (void);
      void (* adw_style_manager_set_color_scheme) (void *self,
                                                   int   color_scheme);
      void* (* adw_style_manager_get_default) (void);
      void *style_manager;

      adw_init = dlsym (adw, "adw_init");
      adw_style_manager_set_color_scheme = dlsym (adw, "adw_style_manager_set_color_scheme");
      adw_style_manager_get_default = dlsym (adw, "adw_style_manager_get_default");

      adw_init ();
      style_manager = adw_style_manager_get_default ();
      adw_style_manager_set_color_scheme (style_manager, 4);
    }
  else
    {
      GtkSettings *settings = gtk_settings_get_default ();
      g_object_set (settings, "gtk-application-prefer-dark-theme", TRUE, NULL);
    }

}

SnowglobeApplication *
snowglobe_application_new (const char        *application_id,
                           GApplicationFlags  flags)
{
  g_return_val_if_fail (application_id != NULL, NULL);

  return g_object_new (SNOWGLOBE_TYPE_APPLICATION,
                       "application-id", application_id,
                       "flags", flags,
                       NULL);
}

static void
snowglobe_application_activate (GApplication *app)
{
  GtkWindow *window;
  SnowglobeApplication *self = SNOWGLOBE_APPLICATION (app);
  g_assert (SNOWGLOBE_IS_APPLICATION (app));

  snowglobe_application_set_dark_style ();

  if (self->main_window_id == 0)
    {
      window = GTK_WINDOW (snowglobe_window_new (GTK_APPLICATION (app),
                                                 self->is_fullscreened,
                                                 self->is_decorated,
                                                 self->emulate_touch,
                                                 self->auto_resize,
                                                 self->show_host_cursor));

      self->main_window_id = gtk_application_window_get_id (GTK_APPLICATION_WINDOW (window));

      start_qemu (self);
    }
  else
    window = gtk_application_get_window_by_id (GTK_APPLICATION (app),
                                               self->main_window_id);

  gtk_window_present (window);
}

static gboolean
snowglobe_application_local_command_line (GApplication   *app,
                                          gchar        ***args,
                                          int            *exit_status)
{
  int i = 1;
  gchar **argv;
  SnowglobeApplication *self = SNOWGLOBE_APPLICATION (app);

  argv = *args;
  /* Used later to start QEMU */
  while (argv[i])
    {
      if (g_strcmp0 (argv[i], "--fullscreen") != 0 &&
          g_strcmp0 (argv[i], "--undecorate") != 0 &&
          g_strcmp0 (argv[i], "--emulate-touch") != 0 &&
          g_strcmp0 (argv[i], "--show-host-cursor") != 0 &&
          g_strcmp0 (argv[i], "--auto-resize") != 0)
        self->argv = g_strsplit (argv[i], " ", -1);
      else if (g_strcmp0 (argv[i], "--fullscreen") == 0)
        self->is_fullscreened = TRUE;
      else if (g_strcmp0 (argv[i], "--undecorate") == 0)
        self->is_decorated = FALSE;
      else if (g_strcmp0 (argv[i], "--emulate-touch") == 0)
        self->emulate_touch = TRUE;
      else if (g_strcmp0 (argv[i], "--show-host-cursor") == 0)
        self->show_host_cursor = TRUE;
      else if (g_strcmp0 (argv[i], "--auto-resize") == 0)
        self->auto_resize = TRUE;
      i++;
    }

  return G_APPLICATION_CLASS (snowglobe_application_parent_class)->local_command_line (app, args, exit_status);
}

static int
snowglobe_application_command_line (GApplication            *app,
                                    GApplicationCommandLine *cmdline)
{
  g_application_activate (app);
  return 0;
}

static void
snowglobe_application_shutdown (GApplication *app)
{
  SnowglobeApplication *self = SNOWGLOBE_APPLICATION (app);

  if (self->proc)
    g_subprocess_force_exit (self->proc);

  G_APPLICATION_CLASS (snowglobe_application_parent_class)->shutdown (app);
}

static void
snowglobe_application_finalize (GObject *object)
{
  SnowglobeApplication *self = SNOWGLOBE_APPLICATION (object);

  if (self->watcher_id > 0)
    g_bus_unwatch_name (self->watcher_id);
  G_OBJECT_CLASS (snowglobe_application_parent_class)->finalize (object);
}

static void
snowglobe_application_dispose (GObject *object)
{
  SnowglobeApplication *self = SNOWGLOBE_APPLICATION (object);
  g_clear_pointer (&self->argv, g_strfreev);

  G_OBJECT_CLASS (snowglobe_application_parent_class)->dispose (object);
}

static void
snowglobe_application_class_init (SnowglobeApplicationClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  object_class->dispose = snowglobe_application_dispose;
  object_class->finalize = snowglobe_application_finalize;

  app_class->activate = snowglobe_application_activate;
  app_class->shutdown = snowglobe_application_shutdown;
  app_class->local_command_line = snowglobe_application_local_command_line;
  app_class->command_line = snowglobe_application_command_line;
}

static void
snowglobe_application_quit_action (GSimpleAction *action,
                                   GVariant      *parameter,
                                   gpointer       user_data)
{
  SnowglobeApplication *self = user_data;
  g_assert (SNOWGLOBE_IS_APPLICATION (self));

  g_application_quit (G_APPLICATION (self));
}

static const GActionEntry app_actions[] = {
  { "quit", snowglobe_application_quit_action },
};

static GOptionEntry options[] =
{
  { "undecorate", 0, 0, G_OPTION_ARG_NONE, NULL,
    N_("Disable window decoration"), NULL },
  { "fullscreen", 0, 0, G_OPTION_ARG_NONE, NULL,
    N_("Start the window fullscreened"), NULL },
  { "emulate-touch", 0, 0, G_OPTION_ARG_NONE, NULL,
    N_("Emulate touch from mouse motion and click events"), NULL },
  { "show-host-cursor", 0, 0, G_OPTION_ARG_NONE, NULL,
    N_("Display host cursor"), NULL },
  { "auto-resize", 0, 0, G_OPTION_ARG_NONE, NULL,
    N_("Enable auto reconfiguring the guest OS's display size when resized"), NULL },
  { NULL }
};

static void
snowglobe_application_init (SnowglobeApplication *self)
{
  self->process_started = FALSE;
  self->is_fullscreened = FALSE;
  self->is_decorated = TRUE;
  self->emulate_touch = FALSE;
  self->show_host_cursor = FALSE;
  self->auto_resize = FALSE;
  g_action_map_add_action_entries (G_ACTION_MAP (self),
                                   app_actions,
                                   G_N_ELEMENTS (app_actions),
                                   self);
  g_application_add_main_option_entries (G_APPLICATION (self), options);
  gtk_application_set_accels_for_action (GTK_APPLICATION (self),
                                         "app.quit",
                                         (const char *[]){ "<primary>q", NULL });
  gtk_application_set_accels_for_action (GTK_APPLICATION (self),
                                         "window.close",
                                         (const char *[]){ "<primary>w", NULL });
  gtk_application_set_accels_for_action (GTK_APPLICATION (self),
                                         "win.fullscreen",
                                         (const char *[]){ "<primary>F11", NULL });
  gtk_application_set_accels_for_action (GTK_APPLICATION (self),
                                         "vm.screenshot",
                                         (const char *[]){ "<primary><alt><shift>S", NULL });
}

