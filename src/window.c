// SPDX-FileCopyrightText: Red Hat, Inc.
// SPDX-License-Identifier: GPL-3.0-or-later

#include "config.h"

#include "window.h"

struct _SnowglobeWindow
{
  GtkApplicationWindow parent_instance;
  /* Motion position */
  double current_y;
  /* Keep track of decoration state */
  gboolean is_decorated;
  gboolean show_host_cursor;
  gboolean auto_resize;
  /* Keep track of emulating touch */
  gboolean emulate_touch;
  gboolean is_touching;

  /* Template widgets */
  MksDisplay    *display;
  GtkRevealer   *header_revealer;
  GtkStack      *stack;
  GtkSpinner    *spinner;
  GtkBox        *status_container;
  GtkImage      *status_image;
  GtkLabel      *status_head_label;
  GtkLabel      *status_body_label;
  GtkStack      *header_stack;
  GtkHeaderBar  *header_empty;
  GtkHeaderBar  *header_display;
  GtkLabel      *subtitle_label;
};

G_DEFINE_FINAL_TYPE (SnowglobeWindow, snowglobe_window, GTK_TYPE_APPLICATION_WINDOW)

static gboolean
update_title_binding (GBinding     *binding,
                      const GValue *from_value,
                      GValue       *to_value,
                      gpointer      user_data)
{
  MksDisplay *display = user_data;
  GdkDisplay *gdisplay;
  GtkShortcutTrigger *trigger;
  g_autofree char *label = NULL;

  g_assert (MKS_IS_DISPLAY (display));

  gdisplay = gtk_widget_get_display (GTK_WIDGET (display));
  trigger = mks_display_get_ungrab_trigger (display);
  label = gtk_shortcut_trigger_to_label (trigger, gdisplay);

  if (g_value_get_boolean (from_value))
    g_value_take_string (to_value, g_strdup_printf ("%s to ungrab", label));

  return TRUE;
}

static void
on_fullscreen_cb (GtkWidget  *widget,
                  const char *action_name,
                  GVariant   *param)
{
  GtkWindow *window;
  SnowglobeWindow *self;
  g_assert (SNOWGLOBE_IS_WINDOW (widget));

  window = GTK_WINDOW (widget);
  self = SNOWGLOBE_WINDOW (widget);

  if (gtk_window_is_fullscreen (window))
    {
      gtk_window_unfullscreen (window);
      gtk_revealer_set_reveal_child (self->header_revealer, FALSE);
    }
  else
    gtk_window_fullscreen (window);

  gtk_widget_grab_focus (GTK_WIDGET (self->display));
}

static void
on_screenshot_cb (GtkWidget  *widget,
                  const char *action_name,
                  GVariant   *param)
{
  SnowglobeWindow *self;
  GskRenderNode *node;
  GskRenderer *renderer;
  GdkTexture *texture;
  g_autoptr (GError) error = NULL;
  GdkClipboard *clipboard;
  GdkPaintable *paintable;
  GtkSnapshot *snapshot;
  int width, height;

  g_assert (SNOWGLOBE_IS_WINDOW (widget));
  g_debug ("Taking a screenshot");

  self = SNOWGLOBE_WINDOW (widget);
  clipboard = gtk_widget_get_clipboard (widget);
  width = gtk_widget_get_width (widget);
  height = gtk_widget_get_height (widget);

  paintable = gtk_widget_paintable_new (GTK_WIDGET (self->display));
  snapshot = gtk_snapshot_new ();
  gdk_paintable_snapshot (paintable, snapshot, width, height);
  node = gtk_snapshot_free_to_node (snapshot);

  renderer = gsk_gl_renderer_new ();
  gsk_renderer_realize (renderer, NULL, &error);
  if (error != NULL)
    {
      g_warning ("Failed to realize a GL renderer %s", error->message);
      return;
    }
  texture = gsk_renderer_render_texture (renderer, node, NULL);
  gsk_renderer_unrealize (renderer);

  gdk_clipboard_set_texture (clipboard, texture);
  gtk_widget_grab_focus (GTK_WIDGET (self->display));
}

static gboolean
keyval_to_keycode (GdkDisplay *display,
                   int         keyval,
                   guint      *keycode)
{
  GdkKeymapKey *keys;
  int n_keys;
  g_assert (GDK_IS_DISPLAY (display));

  if (!gdk_display_map_keyval (display, keyval, &keys, &n_keys))
    {
      g_warning ("Couldn't retrieve keycode");
      return FALSE;
    }
  mks_keyboard_translate (keyval, keys[0].keycode, keycode);
  g_free (keys);
  return TRUE;
}

static void
on_keyboard_cb (GtkWidget  *widget,
                const char *action_name,
                GVariant   *param)
{
  int keyval;
  guint keycodes[3];
  const gchar *accel;
  GdkDisplay *display;
  MksKeyboard *keyboard;
  MksScreen *screen;
  SnowglobeWindow *self;
  g_autoptr (GError) error = NULL;
  accel = g_variant_get_string (param, NULL);
  keyval = gdk_keyval_from_name (accel);

  g_assert (SNOWGLOBE_IS_WINDOW (widget));
  g_assert (keyval != GDK_KEY_VoidSymbol);

  self = SNOWGLOBE_WINDOW (widget);
  screen = mks_display_get_screen (self->display);
  keyboard = mks_screen_get_keyboard (screen);
  g_info ("Triggering keyboard shortcut: Ctrl + Shift + %s", accel);
  display = gtk_widget_get_display (widget);

  if (!keyval_to_keycode (display, GDK_KEY_Control_L, &keycodes[0]))
    return;
  if (!keyval_to_keycode (display, GDK_KEY_Alt_L, &keycodes[1]))
    return;
  if (!keyval_to_keycode (display, keyval, &keycodes[2]))
    return;

  for (guint i = 0; i < 3; i++)
    {
      if (!mks_keyboard_press_sync (keyboard, keycodes[i], NULL, &error))
        {
          g_warning ("Failed to keypress: %s", error->message);
          return;
        }
    }

  for (guint i = 0; i < 3; i++)
    {
      if (!mks_keyboard_release_sync (keyboard, keycodes[i], NULL, &error))
        {
          g_warning ("Failed to key release: %s", error->message);
          return;
        }
    }
  gtk_widget_grab_focus (GTK_WIDGET (self->display));
}

static gboolean
on_timeout_cb (gpointer user_data)
{
  gboolean is_fullscreen;
  g_autoptr (SnowglobeWindow) self = user_data;
  g_assert (SNOWGLOBE_IS_WINDOW (self));
  is_fullscreen = gtk_window_is_fullscreen (GTK_WINDOW (self));
  if (!is_fullscreen)
    return G_SOURCE_REMOVE;

  if (self->current_y > 0)
    {
      gtk_revealer_set_reveal_child (self->header_revealer, FALSE);
      return G_SOURCE_REMOVE;
    }
  return G_SOURCE_CONTINUE;
}

static void
on_motion_cb (SnowglobeWindow *self,
              double           x,
              double           y)
{
  gboolean is_fullscreen;
  g_assert (SNOWGLOBE_IS_WINDOW (self));

  is_fullscreen = gtk_window_is_fullscreen (GTK_WINDOW (self));
  if (!is_fullscreen || !self->is_decorated)
    return;

  self->current_y = y;

  if (y <= FLT_EPSILON)
    {
      gtk_revealer_set_reveal_child (self->header_revealer, TRUE);
      g_timeout_add_seconds (2, on_timeout_cb,
                             g_object_ref (self));
    }
}

static void
snowglobe_window_realize (GtkWidget *widget)
{
  GdkSurface *surface;
  SnowglobeWindow *self = SNOWGLOBE_WINDOW (widget);

  GTK_WIDGET_CLASS (snowglobe_window_parent_class)->realize (widget);

  surface = gtk_native_get_surface (GTK_NATIVE (self));
  g_object_bind_property (surface, "shortcuts-inhibited",
                          self->subtitle_label, "visible",
                          G_BINDING_SYNC_CREATE);
  g_object_bind_property_full (surface, "shortcuts-inhibited",
                               self->subtitle_label, "label",
                               G_BINDING_SYNC_CREATE,
                               update_title_binding, NULL, self->display, NULL);
}

static void
snowglobe_window_class_init (SnowglobeWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  widget_class->realize = snowglobe_window_realize;

  gtk_widget_class_set_template_from_resource (widget_class, "/com/belmoussaoui/snowglobe/window.ui");
  gtk_widget_class_bind_template_child (widget_class, SnowglobeWindow, header_revealer);
  gtk_widget_class_bind_template_child (widget_class, SnowglobeWindow, display);
  gtk_widget_class_bind_template_child (widget_class, SnowglobeWindow, stack);
  gtk_widget_class_bind_template_child (widget_class, SnowglobeWindow, spinner);
  gtk_widget_class_bind_template_child (widget_class, SnowglobeWindow, status_container);
  gtk_widget_class_bind_template_child (widget_class, SnowglobeWindow, status_image);
  gtk_widget_class_bind_template_child (widget_class, SnowglobeWindow, status_head_label);
  gtk_widget_class_bind_template_child (widget_class, SnowglobeWindow, status_body_label);
  gtk_widget_class_bind_template_child (widget_class, SnowglobeWindow, header_stack);
  gtk_widget_class_bind_template_child (widget_class, SnowglobeWindow, header_empty);
  gtk_widget_class_bind_template_child (widget_class, SnowglobeWindow, header_display);
  gtk_widget_class_bind_template_child (widget_class, SnowglobeWindow, subtitle_label);
  gtk_widget_class_bind_template_callback (widget_class, on_motion_cb);

  gtk_widget_class_install_action (widget_class, "win.fullscreen", NULL, on_fullscreen_cb);
  gtk_widget_class_install_action (widget_class, "vm.screenshot", NULL, on_screenshot_cb);
  gtk_widget_class_install_action (widget_class, "vm.keyboard", "s", on_keyboard_cb);
}

static void
snowglobe_window_send_event_cb (GObject      *object,
                                GAsyncResult *result,
                                gpointer      user_data)
{
  MksTouchable *touchable = (MksTouchable *)object;
  g_autoptr (SnowglobeWindow) self = user_data;
  g_autoptr (GError) error = NULL;

  g_assert (MKS_IS_TOUCHABLE (touchable));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (SNOWGLOBE_IS_WINDOW (self));

  if (!mks_touchable_send_event_finish (touchable, result, &error))
    g_debug ("Failed to send touch event: %s", error->message);
}

static gboolean
snowglobe_window_legacy_event_cb (SnowglobeWindow          *self,
                                  GdkEvent                 *event,
                                  GtkEventControllerLegacy *controller)
{
  GdkEventType event_type;
  GdkEventSequence *sequence;
  MksScreen *screen;
  MksTouchable *touchable;
  uint64_t num_slot;
  double guest_x, guest_y;

  g_assert (SNOWGLOBE_IS_WINDOW (self));
  g_assert (GDK_IS_EVENT (event));
  g_assert (GTK_IS_EVENT_CONTROLLER_LEGACY (controller));

  if (!self->emulate_touch)
    return GDK_EVENT_PROPAGATE;

  screen = mks_display_get_screen (self->display);
  touchable = mks_screen_get_touchable (screen);

  event_type = gdk_event_get_event_type (event);
  sequence = gdk_event_get_event_sequence (event);
  num_slot = GPOINTER_TO_UINT (sequence);

  switch ((int)event_type)
    {
    case GDK_MOTION_NOTIFY:
      {
        if (mks_display_get_event_position_in_guest (self->display, event, &guest_x, &guest_y))
          {
            if (self->is_touching)
              {
                mks_touchable_send_event (touchable, MKS_TOUCH_EVENT_UPDATE,
                                          num_slot,
                                          guest_x, guest_y,
                                          NULL,
                                          snowglobe_window_send_event_cb,
                                          g_object_ref (self));
                return GDK_EVENT_STOP;
              }
          }
      }
      break;

    case GDK_BUTTON_PRESS:
    case GDK_BUTTON_RELEASE:
      {
        MksTouchEventKind kind;

        if (event_type == GDK_BUTTON_PRESS)
          {
            kind = MKS_TOUCH_EVENT_BEGIN;
            self->is_touching = TRUE;
          }
        else
          {
            kind = MKS_TOUCH_EVENT_END;
            self->is_touching = FALSE;
          }

        if (mks_display_get_event_position_in_guest (self->display, event, &guest_x, &guest_y))
          {
            mks_touchable_send_event (touchable, kind,
                                      num_slot,
                                      guest_x, guest_y,
                                      NULL,
                                      snowglobe_window_send_event_cb,
                                      g_object_ref (self));
            return GDK_EVENT_STOP;

          }
      }
    default:
      break;
    }
  return GDK_EVENT_PROPAGATE;
}

static void
snowglobe_window_init (SnowglobeWindow *self)
{
  GtkEventController *controller;

  gtk_widget_init_template (GTK_WIDGET (self));
  self->emulate_touch = FALSE;
  self->is_touching = FALSE;

  controller = gtk_event_controller_legacy_new ();
  g_signal_connect_object (controller,
                           "event",
                           G_CALLBACK (snowglobe_window_legacy_event_cb),
                           self,
                           G_CONNECT_SWAPPED);
  gtk_event_controller_set_propagation_phase (controller, GTK_PHASE_CAPTURE);
  gtk_widget_add_controller (GTK_WIDGET (self->display), controller);
}

static void
on_new_session_cb (GObject      *source,
                   GAsyncResult *res,
                   gpointer      user_data)
{
  SnowglobeWindow *self = user_data;
  g_autoptr (GError) error = NULL;
  g_autoptr (MksSession) session = NULL;
  gboolean is_first = TRUE;
  GListModel *devices;
  guint n_items;

  g_assert (SNOWGLOBE_IS_WINDOW (self));
  g_assert (MKS_IS_SESSION (source));
  g_assert (G_IS_ASYNC_RESULT (res));

  session = mks_session_new_for_connection_finish (res, &error);

  if (error != NULL) {
      snowglobe_window_set_status_view (self, NULL,
                                        _("Failed to connect to QEMU DBus"),
                                        error->message);
    } else {
      devices = mks_session_get_devices (g_steal_pointer (&session));
      n_items = g_list_model_get_n_items (devices);

      for (guint i = 0; i < n_items; i++)
        {
          g_autoptr (MksDevice) device = g_list_model_get_item (devices, i);

          if (MKS_IS_SCREEN (device))
            {
              if (is_first)
                {
                  is_first = FALSE;
                  mks_display_set_screen (self->display,
                                          MKS_SCREEN (g_steal_pointer (&device)));

                  gtk_stack_set_visible_child (self->stack,
                                               GTK_WIDGET (self->display));
                  gtk_stack_set_visible_child (self->header_stack,
                                               GTK_WIDGET (self->header_display));

                  gtk_spinner_set_spinning (self->spinner, FALSE);
                  gtk_widget_grab_focus (GTK_WIDGET (self->display));
                }
              else
                {
                  /* Create a new window for each consecutive MksScreen */
                  SnowglobeWindow *window;
                  window = snowglobe_window_new (gtk_window_get_application (GTK_WINDOW (self)),
                                                 gtk_window_is_fullscreen (GTK_WINDOW (self)),
                                                 self->is_decorated,
                                                 self->emulate_touch,
                                                 self->auto_resize,
                                                 self->show_host_cursor);

                  mks_display_set_screen (window->display,
                                          MKS_SCREEN (g_steal_pointer (&device)));

                  gtk_stack_set_visible_child (window->stack,
                                               GTK_WIDGET (window->display));
                  gtk_stack_set_visible_child (window->header_stack,
                                               GTK_WIDGET (window->header_display));

                  gtk_window_present (GTK_WINDOW (window));
                }
            }
        }
    }
}

void
snowglobe_window_set_display_view (SnowglobeWindow  *self,
                                   GDBusConnection  *connection)
{
  g_return_if_fail (SNOWGLOBE_IS_WINDOW (self));
  g_return_if_fail (G_IS_DBUS_CONNECTION (connection));

  mks_session_new_for_connection (connection, G_PRIORITY_DEFAULT,
                                  NULL, on_new_session_cb, self);
}

void
snowglobe_window_set_status_view (SnowglobeWindow *self,
                                  const gchar     *icon_name,
                                  const gchar     *head,
                                  const gchar     *body)
{
  g_return_if_fail (SNOWGLOBE_IS_WINDOW (self));

  gtk_spinner_set_spinning (self->spinner, FALSE);
  gtk_stack_set_visible_child (self->header_stack, GTK_WIDGET (self->header_empty));
  if (icon_name)
    gtk_image_set_from_icon_name (self->status_image, icon_name);

  mks_display_set_screen (self->display, NULL);
  gtk_stack_set_visible_child (self->stack, GTK_WIDGET (self->status_container));
  gtk_label_set_label (self->status_head_label, head);
  gtk_label_set_label (self->status_body_label, body);
}

void
snowglobe_window_set_loading_view (SnowglobeWindow *self)
{
  g_return_if_fail (SNOWGLOBE_IS_WINDOW (self));

  gtk_stack_set_visible_child (self->header_stack, GTK_WIDGET (self->header_empty));
  gtk_stack_set_visible_child (self->stack, GTK_WIDGET (self->spinner));
  gtk_spinner_set_spinning (self->spinner, TRUE);
}

static void
snowglobe_window_set_decorated (SnowglobeWindow *self,
                                gboolean         decorated)
{
  g_return_if_fail (SNOWGLOBE_IS_WINDOW (self));
  g_debug ("Setting decoration to %s", decorated ? "TRUE" : "FALSE");

  self->is_decorated = decorated;
  gtk_widget_set_visible (GTK_WIDGET (self->header_stack), decorated);
  gtk_widget_set_visible (GTK_WIDGET (self->header_revealer), decorated);
  if (!self->is_decorated)
    gtk_widget_remove_css_class (GTK_WIDGET (self), "csd");
}

static void
snowglobe_window_set_emulate_touch (SnowglobeWindow *self,
                                    gboolean         emulate_touch)
{
  g_return_if_fail (SNOWGLOBE_IS_WINDOW (self));
  g_debug ("Setting emulate touch to %s", emulate_touch ? "TRUE" : "FALSE");

  if (emulate_touch)
    mks_display_set_ungrab_trigger (self->display, NULL);
  self->emulate_touch = emulate_touch;
}

static void
snowglobe_window_show_host_cursor (SnowglobeWindow *self,
                                   gboolean         show_host_cursor)
{
  g_autoptr (GdkCursor) cursor = NULL;
  GtkWidget *display_picture;
  g_return_if_fail (SNOWGLOBE_IS_WINDOW (self));
  self->show_host_cursor = show_host_cursor;
  if (show_host_cursor)
    cursor = gdk_cursor_new_from_name ("default", NULL);
  else
    cursor = gdk_cursor_new_from_name ("none", NULL);
  /**
   * We need to change the cursor in MksDisplayPicture which is a child
   * widget of MksDisplay.
  */
  display_picture = gtk_widget_get_first_child (GTK_WIDGET (self->display));

  gtk_widget_set_cursor (display_picture, cursor);
}

static void
snowglobe_window_set_auto_resize (SnowglobeWindow *self,
                                  gboolean         auto_resize)
{
  g_return_if_fail (SNOWGLOBE_IS_WINDOW (self));
  self->auto_resize = auto_resize;
  mks_display_set_auto_resize (self->display, auto_resize);
  gtk_widget_queue_resize (GTK_WIDGET (self->display));
}


SnowglobeWindow *
snowglobe_window_new (GtkApplication *app,
                      gboolean        is_fullscreen,
                      gboolean        is_decorated,
                      gboolean        emulate_touch,
                      gboolean        auto_resize,
                      gboolean        show_host_cursor)
{
  SnowglobeWindow *window;
  window = g_object_new (SNOWGLOBE_TYPE_WINDOW,
                         "application", app,
                         "fullscreened", is_fullscreen,
                         NULL);
  snowglobe_window_set_decorated (SNOWGLOBE_WINDOW (window),
                                  is_decorated);
  snowglobe_window_set_emulate_touch (SNOWGLOBE_WINDOW (window),
                                      emulate_touch);
  snowglobe_window_set_auto_resize (SNOWGLOBE_WINDOW (window),
                                    auto_resize);
  snowglobe_window_show_host_cursor (SNOWGLOBE_WINDOW (window),
                                     show_host_cursor);
  return window;
}
